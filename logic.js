var FAVORITES = [
	{
		"text": "1F469 1F3FE 200D 2764 FE0F 200D 1F48B 200D 1F469 1F3FB",
		"tooltip": "The 35-byte example from \"Bear + Snowflake = Polar Bear\""
	},
	
	{
		"text":"1F441 FE0F 200D 1F5E8 FE0F",
		"tooltip":"Eye in speech bubble"
	},
	
	{
		"text":"2764 FE0F 200D 1F525",
		"tooltip":"Heart on fire"
	},
	
	{
		"text":"1F468 1F3FD 200D 1F9B1",
		"tooltip":"Skin tone and hair modifiers"
	},
	
	{
		"text":"1F469 1F3FB 200D 1F393",
		"tooltip":"Skin tone and clothing modifiers"
	},
	
	{
		"text":"1F468 1F3FC 200D 1F4BB",
		"tooltip":"Programmer!"
	},
	
	{
		"text":"1F469 200D 1F9AF",
		"tooltip":"Person with cane"
	},
	
	{
		"text":"1F468 1F3FD 200D 1F469 1F3FB 200D 1F467 1F3FB 200D 1F466 1F3FD 200D 1F466 1F3FB",
		"tooltip":"Family of 5"
	},
	
	{
		"text":"1F415 200D 1F9BA",
		"tooltip":"Dog + Vest = Guide Dog"
	},
	
	{
		"text":"38 FE0F 20E3",
		"tooltip":"Number keycap"
	},
	
	{
		"text":"1F3F4 200D 2620 FE0F",
		"tooltip":"Pirate Flag"
	},
	
	{
		"text":"1F1F8 1F1EC",
		"tooltip":"Country's Flag"
	}
]

var textbox;
var output;
var output_emoji;
var palette_div;
var palette_start = "1F300";
var range_indicator;
var rollIntvl;
var tooltipDiv;

var emojiToggle;
var isEmojiModeOn = true;

window.onload = function() {
	textbox = document.getElementById("textbox");
	output = document.getElementById("output");
	output_emoji = document.getElementById("output_emoji");
	textbox.oninput = updateContents;
	
	emojiToggle = document.getElementById("emojiToggle");
	emojiToggle.onclick = function() {
		isEmojiModeOn = !isEmojiModeOn;
		if (isEmojiModeOn) {
			emojiToggle.setAttribute("class", "emojiToggleChecked");
		}
		else {
			emojiToggle.setAttribute("class", "");
		}
		
		buildPalette(palette_start)
	}
	
	range_indicator = document.getElementById("rangeIndicator");
	palette_div = document.getElementById("palette");
	buildPalette("1F300");
	
	window.onmouseup = stopRoll;
	
	tooltipDiv = document.getElementById("tooltipDiv");
	window.onmousemove = function(e) {
		tooltipDiv.style.top = (e.clientY - 70) + "px";
		tooltipDiv.style.left = e.clientX + "px";
	}
}

function updateContents() {
	try {
		var txt = textbox.value;
		var codes = txt.split(" ");
		
		output.value = "";
		for (var c of codes) {
			if (c == "|") {
				output.value += "\u200B";
				continue;
			}
			
			try {
				output.value += String.fromCodePoint(parseInt(c,16));
			}
			catch (err) {
				// do nothing
			}
		}
	}
	catch (err) {
		output.value = "Error";
		console.log(err);
	}
	finally {
		output_emoji.value = output.value;
	}
}

function buildPalette(start) {
	var table = document.createElement("table");
	var start = parseInt(start,16); 
	
	for (var i=start; i<start+128; i+=16) {
		var row = document.createElement("tr");
		for (var j=i; j<i+16; j++) {
			var cell = document.createElement("td");
			cell.innerHTML = "&#" + j;

			if (isEmojiModeOn) {
				cell.innerHTML += "&#xFE0F";
				cell.setAttribute("class", "paletteMember td-emojiOn");
			}
			else {
				cell.innerHTML += "&#xFE0E";
				cell.setAttribute("class", "paletteMember td-emojiOff");
			}
			
			cell.setAttribute("code_point", j);
			cell.onclick = palette_choice;
			cell.onmouseover = function() {
				var pt = parseInt(this.getAttribute("code_point")).toString(16).toUpperCase();
				tooltip(pt);
			};
			cell.onmouseout = clearTooltip;
			row.appendChild(cell);
		}
		table.appendChild(row);
	}
	palette_div.innerHTML = "";
	palette_div.appendChild(table);
	
	range_indicator.innerHTML = start.toString(16).toUpperCase() + " - ";
	range_indicator.innerHTML += (start+128).toString(16).toUpperCase();
}

function showFavorites() {
	var table = document.createElement("table");
	
	for (var i=0; i<64; i+=8) {
		var row = document.createElement("tr");
		for (var j=i; j<i+8; j++) {
			var cell = document.createElement("td");
			if (j < FAVORITES.length) {
				cell.innerHTML = "";
				for (var c of FAVORITES[j]["text"].split(" ")) {
					cell.innerHTML += String.fromCodePoint(parseInt(c,16));
				}
				
				cell.setAttribute("code_point", FAVORITES[j]["text"]);
				cell.setAttribute("tooltip", FAVORITES[j]["tooltip"]);
				cell.onclick = favorite_choice;
				cell.onmouseover = function() {
					var pt = this.getAttribute("tooltip");
					tooltip(pt);
				};
				cell.onmouseout = clearTooltip;
			}
			else {
				cell.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			}

			if (isEmojiModeOn) {
				cell.innerHTML += "&#xFE0F";
				cell.setAttribute("class", "paletteMember td-emojiOn");
			}
			else {
				cell.innerHTML += "&#xFE0E";
				cell.setAttribute("class", "paletteMember td-emojiOff");
			}
			
			row.appendChild(cell);
		}
		table.appendChild(row);
	}
	palette_div.innerHTML = "";
	palette_div.appendChild(table);
	
	range_indicator.innerHTML = "Favorites";
}

function favorite_choice() {
	textbox.value = this.getAttribute("code_point");
	cleanupText();
	updateContents();
}
	
function promptJump() {
	var res = prompt("Enter unicode destination in hex", palette_start);
	if (!isNaN(parseInt(res,16))) {
		paletteJump(res);
	}
}

function paletteJump(location) {
	var num = parseInt(location, 16);
	if (num < 0) num = 0;
	num = parseInt(num / 128) * 128;
	console.log(num);
	
	palette_start = num.toString(16);
	buildPalette(palette_start);
}

function paletteOffset(amount) {
	var start = parseInt(palette_start,16) + amount;
	paletteJump(start.toString(16));
}

function palette_choice() {
	var codepoint_dec = parseInt(this.getAttribute("code_point"));
	var codepoint_hex = codepoint_dec.toString(16);
	add_symbol(codepoint_hex);	
}

function add_symbol(hex) {
	var text = textbox.value;
	
	var idx = textbox.selectionStart;
	var left = text.substring(0, idx);
	var right = text.substring(idx);
	
	console.log(idx);
	
	if (idx != 0 && text.charAt(idx-1) != " ") {
		hex = " " + hex;
	}
	if (text.charAt(idx) != " ") {
		hex += " ";
	}
	
	textbox.value = left + hex + right;
	cleanupText();
	updateContents();
}

function textboxDelete() {
	var text = textbox.value;
	
	var idx = textbox.selectionStart;
	
	var wordEnd = idx-1;
	while (wordEnd < text.length && text.charAt(wordEnd) != " ") {
		wordEnd += 1;
	}
	
	var wordStart = idx-2;
	while (wordStart > 0 && text.charAt(wordStart) != " ") {
		wordStart -= 1;
	}
	
	textbox.value = text.substring(0,wordStart) + text.substring(wordEnd);
	updateContents();
}

function startRoll(amount) {
	rollIntvl = setInterval(function() {paletteOffset(amount)}, 200);
}

function stopRoll() {
	clearInterval(rollIntvl);
}

function cleanupText() {
	var text = textbox.value.toUpperCase();
	
	if (text.charAt(text.length-1) == " ") {
		text = text.trim() + " ";
	}
	else {
		text = text.trim();
	}
	
	textbox.value = text;
}

function tooltip(s) {
	tooltipDiv.innerHTML = s;
	tooltipDiv.style.display = "inline-block";
}

function clearTooltip() {
	tooltipDiv.style.display = "none";
}

function openModal() {
    document.getElementById("modal").style.display = "block";
}

function closeModal() {
    document.getElementById("modal").style.display = "none";
}