# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Emoji is Fun!**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/q3ZJxlJFcs8/0.jpg)](https://youtu.be/q3ZJxlJFcs8 "Click to Watch")

To access the live version of this site to be run in your browser, visit the NERDfirst website: https://resources.nerdfirst.net/emoji


# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.